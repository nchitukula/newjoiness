package neha;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * class for implementing the methods of SQL from jdbc
 * @param tablename
 * @return
 */
public class jdbc {
	static final String DB_URL = "jdbc:mysql://localhost/neha";
	static final String USER = "root";
	static final String PASS = "root";
	/**
	 * Helper method for creating table 
	 * @param tablename
	 * @return
	 */
	
	public boolean CreateTable(String tablename) {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "CREATE TABLE newjoinees " +
	                   "(username varchar(255) , " +
	        		  "id int,"+
	                   " otp int, " + 
	                   " recovery_code int , " + 
	                   " firstname varchar(255), "+
	                   " lastname varchar(255),"+
	                   "department varchar(255),"+
	                   "location varchar(255))";
	                    
	          //System.out.println(sql);
	         stmt.executeUpdate(sql);
	         System.out.println("Created table in given database...");  
	         return true;
	      } catch (SQLException e) {
	         e.printStackTrace();
	      } 
	   
	return false;
	}
 /**
  * Helper method for Inserting data into a file.
  * 
  * @return
  */

public boolean InsertData() {
	try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Statement stmt = conn.createStatement();) {
		
		
		return true;// values insertion successful
	} catch (SQLException ex) {
		ex.printStackTrace();
	}
	return false;// fails to insert values
}
	}





		
