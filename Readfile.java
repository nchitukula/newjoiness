package neha;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Implementing the class Readfile to read given data from .txt file.
 * 
 * @author nchitukula
 *
 */

public class Readfile {
	File file = new File("C:\\Users\\nchitukula\\neweclipseworkspace22\\NewJoiness\\src\\neha\\newjioness.txt");

	/*
	 * helper method to read data from the file
	 */
	public String readData(String filename) {

		String filedata = " ";
		try {
			Scanner reader = new Scanner(file);

			while (reader.hasNextLine()) {
				filedata += reader.nextLine() + "\n";

			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filedata;

	}
}
